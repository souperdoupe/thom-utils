#!/bin/sh

# firmware-dl.sh
#
# 	Downloads proprietary firmware .debs
#
#	(Intended for Devuan, but should work on any system with apt.)

# Proprietary firmware directory
pfdir="$1"

# Firmware list
firm="/tmp/firmware.list"

# Apt-cache search package names. These will be grepped against the
# string "firmware"
pkglist="wireless b43 intelwimax bnx"

# Function to trim the fat off .deb listings
trim(){
	sed 's/ - .*//g'
}

# Bleach the old firmware list
if [ -f $firm ]; then 
	cat /dev/null > /tmp/firm;
fi

# Aggregate a list of packages. Note: Ignores versions.
for i in $pkglist; do
	apt-cache search $i | grep firmware | trim >> $firm
done

cd $pfdir

# Loop through each hit with the keyword wireless and firmware
for i in $(cat $firm); do

	# Make sure it doesn't already exist
	if [ ! $(ls | grep "$i") ]; then
		# If it isn't already there, then download it
		apt-get download $i;
	fi

done

# Cleanup
rm /tmp/firmware.txt
