#!/bin/bash

# Get the wifi signal/SSID.
#
# The purpose and functionality is identical to
# wifi_check.py. This script is meant for anyone
# who doesn't have python, or cannot use that
# program for whatever reason.

chkStr()
{
	local sig=$1

	# Lower thresholds for signal strengths.
	local excellent=-30
	local good=-67
	local poor=-90

	# Excellent signal.
	if [[ "$(expr $sig \> $excellent)" == "1" ]];
	then
		echo "=";

	# "Good" signal strength.
	elif [[ "$(expr $sig \> $good)" == "1" ]];
	then
		echo "";
	elif [[ "$(expr $sig \> $poor)" == "1" ]];

	# Return the SSID if there's signal.
	then
		echo "!";

	# Likely, no signal strength after 90dBm.
	else
		echo "";
	fi
}

chkAp()
{
	# Evaluate the access point.
	local raw_ap="$( \
		/sbin/iwconfig wlan0 | \
		grep ESSID | \
		sed -r 's/.*ESSID:"(.*)".*/\1/'
	)"
	# When no ESSID connected, col. 4 is always "off/any."
	if [[ "$(echo $raw_ap | awk '{print $4}')" == \
		"ESSID:off/any" ]];
	then
		echo "offline";
	else
		# Return a shortened version.
		echo $raw_ap | head -c 8;
	fi
}

main()
{
	# Signal strength.
	local sig=$( \
		cat /proc/net/wireless | \
		grep wlan0 | \
		awk '{print $4}' | \
		sed 's/\.//g' \
	)

	# Get the ESSID, or "offline."
	local ap=$(chkAp);

	# Get a symbol representing signal strength.
	local str="$(chkStr $sig)"

	case $1 in
		# Also display the signal strength in dBm.
		-a|--all)
			local dsig="($sig)"
		;;
		
		# Display only the signal.
		-s|--signal)
			local dsig="$sig"
			local ap=""
			local str=""
		;;
	esac

	# Return a desired value.
	echo "$str$ap$dsig"
}

main "$1"
