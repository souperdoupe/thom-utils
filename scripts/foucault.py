#!/usr/bin/python3

'''
foucault.py

    Display information about your active connections.
    
    This script just filters netstat. Then, its shows information about
    the organization that owns each server.
    
    This started as a shell script. Take that as you will.
'''

import subprocess
import ipaddress
import time
import datetime
from os import system


class Tools:
    
    # Shortcut to run shell commands with output.
    def shellCmd(self, cmd):
        
        output = subprocess.check_output(cmd, shell=True, 
                                    stderr=subprocess.DEVNULL).decode(
                                    "utf-8")
        return output
    
    # Shortcut for a date_time stamp.
    def timeStamp(self, fmt='%Y-%m-%d_%H-%M-%S'):
    
        date = datetime.datetime.now().strftime(fmt)
        return date
    
    # Log input.
    def log(self, content):
        
        with open('/tmp/panopticon.log', 'a') as log:
            log.write(content)


# Monitor your connection details.
class Foucault:
    
    @classmethod
    def __init__(cls, mode):
        # Import the tools.
        cls.t = Tools()
        
        # Accepts 'unique' or 'new'.
        cls.mode = mode
        
        # Hold new IPs and data.
        cls.new_ip_list = {}
        
        # Hold unique IPs and data.
        cls.unique_ip_list = {}
    
    # Gets a whois value.
    @classmethod
    def get_whois_info(cls, ip, whois_attr):
    
        try:
            # Build the whois command.
            net_name = "whois " + ip + " | grep -i " + whois_attr
            
            # Get the value. (Omit anything before the colon.)
            net_name = cls.t.shellCmd(net_name).split()[1]
            
        except Exception as err:
            net_name = "error"
        
        return net_name
    
    # Pull the IP address from netstat data.
    @classmethod
    def get_ip(cls, netstat_data):
        return netstat_data[:netstat_data.rfind(":")].split(",")[0]
    
    # Update data in the list of new IPs. NOTE: For this to work
    # properly, pass a line from netstat, which contains the IP and PID.
    @classmethod
    def is_valid_ip(cls, ip_pid):
        
        is_ip = False
        
        # Get the IP address. (Splits the ip [0] from the pid [0].)
        new_ip = cls.get_ip(ip_pid)

        try:
            # Verify that the IP address is valid.
            if ipaddress.ip_address(new_ip):
                is_ip = True
        
        except Exception as err:
            pass
            
        return is_ip

    @classmethod
    def write_new_data(cls, ip_pid):
        
        # Get the remote IP address.
        remote_ip = cls.get_ip(ip_pid)
        
        # Get the PID/process name.
        process = ip_pid.split(",")[1]
        
        # Get the company/organization name.
        group = cls.get_whois_info(remote_ip, "Organization")
        
        # Try another string if "Organization" doesn't work.
        if (group == "error"):
            group = cls.get_whois_info(remote_ip, "org-name")
            
        # Write data to the new list.
        cls.new_ip_list[remote_ip] = [group, process]

    # Generate a list of new connections and related data.
    @classmethod
    def gen_new_ips(cls):
        
        # Flush the new IP dictionary.
        cls.new_ip_data = {}
        
        # Run a shell command to get remote IP and PID/process name.
        ip_and_process = """
                    netstat -punt | \
                    grep -v TIME_WAIT | \
                    grep -v TIME_CLOSE | \
                    grep -v FIN_WAIT1 | \
                    awk '{print $5","$7}' | \
                    sort | \
                    uniq
                    """
        # Create a list of IP entries.
        ip_raw = cls.t.shellCmd(ip_and_process)
        
        # Loop through those entries.
        for i in ip_raw.splitlines():
                        
            # ...and update the data for valid IPs.
            if (cls.is_valid_ip(i) == True):
                cls.write_new_data(i)

    # Evaluate if a new IP is given.
    @classmethod
    def is_unique_ip(cls, ip):
                
        if ip not in cls.unique_ip_list:
            return True
            
    # Update the list of unique IP addresses.
    @classmethod    
    def get_unqiue_ips(cls):
        
        # Update the open connections
        cls.gen_new_ips()
                
        # Loop through the IP list.
        for ip in cls.new_ip_list.keys():
                                    
            # Input only IPs that are not already there.            
            if (cls.is_unique_ip(ip) == True):
                
                # Append the key and values to the unique list.
                cls.unique_ip_list[ip] = cls.new_ip_list[ip]

    # Print unique IPs to the console.
    @classmethod
    def console_ui(cls):
        
        if (cls.mode == "unique"):
            ip_list = cls.unique_ip_list
            cls.get_unqiue_ips()

        elif (cls.mode == "new"):
            ip_list = cls.new_ip_list
            cls.gen_new_ips()

        system('clear')
        
        for ip in ip_list.keys():
            
            # Get the process name/PID
            process = ip_list[ip][0]
            
            # ...and the group name.
            name = ip_list[ip][1]
            
            print(ip + "\t\t" + name + "\t" + process)
    
    # Montior and log unique connections.
    @classmethod
    def panopticon(cls):
        
        system('clear')
        
        while True:
            try:            
                cls.console_ui()
                time.sleep(2)
            
            except KeyboardInterrupt as err:
                print("Exiting...")
                break


if __name__ == '__main__':
    f = Foucault('unique')
    f.panopticon()
