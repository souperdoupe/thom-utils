#!/usr/bin/env python3
'''
wifi_signal.py

    Check your wifi signal. Intended for use in a lightweight statusbar,
    like dwm or tint2.
    
Notes:

    This could be done with a shell script. But, I ran into issues
    where the script would mess up its handling of the data. (For
    example, it would break if no info were found.) As opposed to
    writing a shell script with advanced error-catching, I opted instead
    to port it to a Python app.
    
    For a more developed, feature-rich network monitor, see the
    referenced programs.
    
See also:

    wavemon     Full-featured UI for your network traffic
    kismet      Details about many network types and information
'''

import os
import re
import time
import sys
from PyQt5.QtGui import * 
from PyQt5.QtWidgets import * 
from PyQt5 import QtCore

class WifiSignal:
    
    @classmethod
    def __init__(cls, proc_file, wifi_iface):
        
        cls.proc_file = proc_file
        cls.wifi_iface = wifi_iface
        cls.disconnected_msg = "off"
        cls.signal = cls.disconnected_msg
    
    @classmethod
    def get_signal(cls):
        
        with open('/proc/net/wireless') as file:
        
            for line in file:
            
                cls.eval_line(line)
                

    @classmethod
    def eval_line(cls, line):

        line = line.split()

        is_wlan = re.sub(r'\W+', '', line[0])
                
        if is_wlan == "wlan0":
            cls.test_signal(line[3])
            
    @classmethod
    def test_signal(cls, signal):
        
        # Store the signal as a negative value.
        signal = int("-" + re.sub(r'\W+', '', signal))
        
        # Signals lower than -89dBm are usually disconnected.
        if signal > -89:
            cls.signal = signal
                
        else:
            cls.signal = cls.disconnected_msg
        
    @classmethod
    def print_procfile(cls):
        
        with open(proc_file) as procfile:
            for line in procfile:
                print(line)


class TrayIcon:
    
    @classmethod
    def __init__(cls, proc_file, wifi_iface):
        cls.w = WifiSignal(proc_file, wifi_iface)
    
    @classmethod
    def display_tray_icon(cls):
        
        # Get signal strength.
        cls.w.get_signal()
        print(str(cls.w.signal))
        
        app = QApplication([]) 
        app.setQuitOnLastWindowClosed(False) 
        
        # Adding item on the menu bar 
        tray = QSystemTrayIcon()
          
        # Creating the options 
        menu = QMenu() 
        option1 = QAction("Lazy") 
        option2 = QAction(
            "Signal: " + str(cls.w.signal)) 
        menu.addAction(option1) 
        menu.addAction(option2) 
          
        # To quit the app 
        quit = QAction("Quit") 
        quit.triggered.connect(app.quit) 
        menu.addAction(quit) 
          
        # Adding options to the System Tray 
        tray.setContextMenu(menu) 
            
        # Adding an icon 
        if cls.w.signal != cls.w.disconnected_msg:
            icon = QIcon("wifi_icon.png")
            
        else:
            icon = QIcon("wifi_disconnected.png")
         
        tray.setIcon(icon)
        tray.setVisible(True) 

        timer = QtCore.QTimer(interval=5*1000)              
        timer.start()
        sys.exit(app.exec_())
        print("ABOUT TO SLEEP")
        

if __name__ == "__main__":
    
    procfile = "/proc/net/wireless"
    wlan = "wlan0"
    
    w = WifiSignal(procfile, wlan)
    
    w.get_signal()
    print("[" + str(w.signal) + " dBm]")
    
    if sys.argv[1] == "-t":
        t = TrayIcon(procfile, wlan)
        t.display_tray_icon()
