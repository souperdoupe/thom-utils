# thomos-scripts

For best results, save to /dev/null.

# Screenshots

`foucault.py`: Monitors network connections, automates whois calls.

![](./screenshots/foucault.png)

`datestamp`: Print the date in a way that's easy to manipulate.

![](./screenshots/datestamp.png)

`sleep-evaluator`: Determine how long a system is inactive (no input).

![](./screenshots/sleep-evaluator.png)

`wifi-chk`: Check your current SSID or signal strength.

![](./screenshots/wifi-chk.png)
