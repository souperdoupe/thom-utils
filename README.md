# thom-utils

Minimalist utilities for an impermanent userspace.

These are designed for a Devuan environment.  They should work fine with
other distributions, but may require some adjustment.

# Intended use

These tools are meant to support your desired desktop. They are
the result of bypassing broken or neglected tools that claimed to do the
same thing.  Over time, they also became a means to port various 
functionalities across different distributions.

## Sample usage

1.	Choose a Linux distro. (ThomOS is built on Devuan, deboostrapped.)
2.	Install a minimal version of your distro (eg, debootstrap, mininstall, whatever.)
3.	`cd /dir/for/build`
3.	`git clone https://gitlab.com/souperdoupe/thom-utils`
4.	Run the installer script or makefile in the directories that contain whatever tool(s) you want.

# See also:

-	[ThomOS](https://gitlab.com/souperdoupe/thomos) - minimalist userspace 
	built with thom-utils

-	[crunkbong](https://souperdoupe.github.io) (antequated) - precursor 
	to thom-utils/ThomOS
