/*
 * Modified an existing scanner, written by a true hero. Info about the
 * original:
 * 
 *	> wifi-scan-all example for wifi-scan library
 *	> 
 *	> Copyright (C) 2016 Bartosz Meglicki <meglickib@gmail.com>
 *	> 
 *	> This program is free software; you can redistribute it and/or modify
 *	> it under the terms of the GNU General Public License version 3 as
 *	> published by the Free Software Foundation.
 *	> This program is distributed "as is" WITHOUT ANY WARRANTY of any
 *	> kind, whether express or implied; without even the implied warranty
 *	> of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	> GNU General Public License for more details.
 */

#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include "./wifi_scan.h"
#include "scan_util.h"
#include "cmd.h"
extern Defaults def;


// Convert bssid to printable hardware mac address
const char *bssid_to_string(
				const uint8_t bssid[BSSID_LENGTH],
				char bssid_string[BSSID_STRING_LENGTH])
{
	snprintf(bssid_string, 
			BSSID_STRING_LENGTH, "%02x:%02x:%02x:%02x:%02x:%02x",
			bssid[0], bssid[1], bssid[2], bssid[3], bssid[4], bssid[5]);
	
	return bssid_string;
}

// type: 0=scan, 1=lazy
const char * scan_util(int type)
{
	// The maximum amounts of APs.
	const int BSS_INFOS = 10; 

	// Stores library information
	struct wifi_scan *wifi = NULL;    

	// Keep AP information.
	struct bss_info bss[BSS_INFOS];

	// Convert BSSID to printable hardware mac address
	char mac[BSSID_STRING_LENGTH];

	int status, i;
	
	// Initialize the library and get network statuses.
	wifi = wifi_scan_init(def.iface);
	
	status = wifi_scan_all(wifi, bss, BSS_INFOS);
	
	wifi_scan_close(wifi);
	
	// If the wifi device is "unreachable," wait and try again.
	if (status < 0) {
		
		perror("Unable to get scan data.\n");
	
	// Lazy mode. NOTE: Should return a string (ssid name).
	} else if (type == 1) {
		
		// Loop through all directories.
		struct dirent *de;
		DIR *dr = opendir(def.config_path);
		
		if (dr == NULL) {
			printf("Could not open directory.\n");
			return "nope";
		}
		
		while ((de = readdir(dr)) != NULL)
		{
			// Pointer type used to make an active connection.
			char *item = de -> d_name;
			
			for (int i=0; i < status && i< BSS_INFOS; ++i)
			{
				if (!strcmp(item, bss[i].ssid)) {
					
					// Stop looping once a saved SSID is found.
					closedir(dr);
					printf("%s was found.\n", item);
					return item;	
				}
			}
		}
		// Return an error if nothing is found.
		return "Nothing found.\n";
		
	// Scan available APs.
	} else if (type == 0) {
	
		// Returns the number of found stations. It may be greater 
		// than BSS_INFOS; that's why we test for both in the loop.
		for(i = 0; i < status && i < BSS_INFOS; ++i)
		{
			printf("%s %s  %d dBm on frequency %u MHz seen %d ms ago status %s\n",
			   bssid_to_string(bss[i].bssid, mac), 
			   bss[i].ssid,  
			   bss[i].signal_mbm/100, 
			   bss[i].frequency,
			   bss[i].seen_ms_ago, 
			   (bss[i].status==BSS_ASSOCIATED ? "associated" : "")
			);
		}
		return "\n";
	}
}

