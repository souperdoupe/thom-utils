/**
 * daemon.h
 * 
 * Headers for the wefe daemon.
 */
 
#include <sys/socket.h> 
#include <netinet/in.h> 

// daemon "class" variables.
typedef struct daemon_args_s
{
	int server_fd, new_socket, valread, status, opt, addrlen; 
	struct sockaddr_in address;	
	char mode[1024], option[1024];
	const char *reply;

} daemon_args_t;

typedef daemon_args_t* DaemonArgs;

DaemonArgs make_daemon_args();

// Create the initial connection.
void build_connection(DaemonArgs d);

// Reset variables, etc.
void cleanup(DaemonArgs d);
void clear_messages(DaemonArgs d);

// Main function.
void daemonized();

// Send a reply back to the client application.
void send_reply(DaemonArgs d);

// Wait for data from the client.
void wait_for_message(DaemonArgs d);
