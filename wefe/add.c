/**
 * Add a network.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <curses.h>

#include "add.h"
#include "cmd.h"
extern Defaults def;


// Generate a wpasupplicant config.
int add(const char * const ssid)
{
	AddArgs ad = make_add_args();
	int r = 0;
	
	strcat(ad->ssid, ssid);
	
	get_password(ad);
	
	make_config_file(ad);
	
	// Reset the password data.
	memset(ad->pass, 0, sizeof(ad->pass));
	
	r = ad->r;
	free(ad);
	
	return r;
}

static AddArgs make_add_args()
{
	AddArgs args = malloc(sizeof(add_args_t));
	
	args->size = 256;
	args->r = 0;
	
	return args;
}

static void get_password(AddArgs ad)
{
	// Use ncurses to hide the passphrase.
	initscr();
	printw("Enter the passphrase for %s. If this is an open network, " \
			"enter 'open' (no quotes).\n", ad->ssid);
	noecho();

	// Store the password.
	getstr(ad->pass);
	echo();
	endwin();
	
	return;
}

// Generate a wpasupplicant config.
static void make_config_file(AddArgs ad)
{
	if (strcmp(ad->pass, "open") == 0)
	{		
		make_open_config(ad);
	}
	else
	{
		make_wpa_config(ad);
	}

	return;
}

static void make_wpa_config(AddArgs ad)
{
	// Build a wpa_passphrase command.
	snprintf(ad->config_cmd, ad->size,
		"/usr/bin/wpa_passphrase '%s' '%s' > '%s'/'%s'",
		ad->ssid, ad->pass, def.config_path, ad->ssid);

	// Execute the command.			
	ad->r = system(ad->config_cmd);
	
	return;
}

static void make_open_config(AddArgs ad)
{
	// Get the config path.
	snprintf(ad->config_cmd, ad->size, "%s/%s", def.config_path, ad->ssid);

	FILE *conf_file = fopen(ad->config_cmd, "w+");
		
	// Save the word 'open.'
	ad->r = fprintf(conf_file, "open");
	
	// Non-negative/zero values are success.
	if (ad->r > 0) {
		ad->r = 0;
	}
		
	fclose(conf_file);
		
	return;
}
