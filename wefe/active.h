/**
 * active.h
 * 
 * Headers for active.c
 */
 
// Use char arrays, not pointers, because of snprintf.
typedef struct active_args_s
{
	char conn[128];
	char dhc[128];
	char ssid[33];
	int net_type;
	int bsize;
	int r;
		
} active_args_t;

typedef active_args_t* ActiveArgs;

// Prototypes.
int active(const char * const ssid);
static ActiveArgs make_active_args();
static void build_conn(ActiveArgs a);
static void build_dhc(ActiveArgs a);
static void set_open_cmd(ActiveArgs a);
static void set_wpa_cmd(ActiveArgs a);
static void get_net_type(ActiveArgs a);
