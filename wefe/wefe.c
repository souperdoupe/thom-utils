/* wefe - Network assistant
* Developed by souperdoupe: https://gitlab.com/souperdoupe
* Inspired by Ceni and the FreeBSD wifi manager.

WIP:
* Create a working release 0.1
* Read wefe structure as an array from config.h, suckless-style 
*  (replace wefe.conf)
* Call pre/post scripts using an sha256 hash (ala sup)
* Using bmegli's wifi-scan library for the new scanner.
* 
* -	https://github.com/bmegli/wifi-scan
*
* Look-ahead:
* -	Associate the external program names with an sha256 hash
*/

#include <stdio.h>
#include <string.h>

// Custom headers.
#include "execute.h"
#include "daemon.h"


// Run wefe as daemon, or execute a single command.
int main(const int argc, const char *argv[])
{	
	int r = 0;
	
	if (strcmp(argv[1], "-d") == 0)
	{
		// Launch the daemon.
		daemonized();		
	}
	
	else
	{
		// Execute commands normally.
		r = execute(argv[1], argv[2]);
	}
	return r;
};
