/**
 * kill.c
 * 
 * 	Clear a network interface.
 *	
 *	An interface successfully flushes in this order:
 * 	-	Kill wpasupplicant/dhclient
 * 	-	Take down the interface (also clears the SSID)
 * 	-	Flush the routes
 * 	-	Bring the interface back up
 */

#include <stdio.h>
#include <stdlib.h>
#include "kill.h"
#include "cmd.h"
extern Defaults def;


// Kill the active connection.
int kill()
{	
	int r = 0;
	
	KillArgs k = get_kill_args();
	k->r = 0;
		
	build_kill_cmds(k);

	run_kill_cmds(k);

	r =  k->r;
	free(k);
	
	return r;
};

static KillArgs get_kill_args()
{
	KillArgs k = malloc(sizeof(kill_args_t));
	return k;
}

static void build_kill_cmds(KillArgs k)
{
	int b = 128;
	
	// Build the commands.
	snprintf(k->dhclient, b, "%s dhclient", def.killall);
	snprintf(k->wpasup, b, "%s wpa_supplicant", def.killall);
	
	// Flush routes.
	snprintf(k->ip_flush, b, "%s addr flush %s", def.if_config, 
			def.iface);
	
	// This would change for ifconfig.
	snprintf(k->if_inet, b, "%s addr add 0.0.0.0 dev %s",
			def.if_config, def.iface);

	// Bring down interface.
	snprintf(k->if_down, b, "%s link set down dev %s", 
			def.if_config, def.iface);
	
	// Bring up interface.
	snprintf(k->if_up, b, "%s link set up dev %s", 
			def.if_config, def.iface);
		
	return;
}

static void run_kill_cmds(KillArgs k)
{
	// Try to kill dhclient and wpasupplicant first.
	k->r = system(k->dhclient) + system(k->wpasup);
	
	// Codes 256/512 just mean that one/both were not running.
	if ((k->r == 0) || (k->r == 256) || (k->r == 512))
	{
		// Declare success.
		k->r = 0;
		
		// Try to take down the interface...
		system(k->if_down);
		
		// ...then, run the other commands, very inelegantly...
		k->r = system(k->ip_flush) + system(k->if_inet);
		
		// ...and pull the interface back up.
		system(k->if_up);
		
		// Error killing the supplemental commands.
		if (k->r < 0)
		{
			k->r = -21;
		}
	}
	// Error killing dhclient/wpasupplicant.
	else
	{
		k->r = -20;
	}
	return;
}
