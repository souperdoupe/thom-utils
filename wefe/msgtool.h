/*
 * msgtool.h
 *
 * 	interperet wefe return codes.
 * 
 * 0: Success
 * 	-	1: active
 *  -	2: on
 * 	- 	3: kill
 *  -	4: add
 *  -	5: scan
 *  -	6: lazy
 * 
 * 10: active errors:
 * 	-	10: Empty config
 *  -	11: Failed to build connection command.
 */


const char * const get_msg(int code)
{
	switch(code)
	{
		case 0:
			"Success!";
			break;
