/*
 * kill.h
 * 
 * 	Clear a network interface.
 */

typedef struct kill_args_s
{
	char if_inet[128];
	char if_down[128];
	char if_up[128];
	char wpasup[128];
	char dhclient[128];
	char ip_flush[128];
	int r;
	
} kill_args_t;

typedef kill_args_t* KillArgs;

int kill();
static KillArgs get_kill_args();
static void build_kill_cmds(KillArgs k);
static void run_kill_cmds(KillArgs k);
