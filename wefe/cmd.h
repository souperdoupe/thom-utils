#ifndef __CMD_H
#define __CMD_H

// Use for setting commands and paths.
typedef struct GlobalTools
{
	const char * const iface;
	const char * const config_path;
	const char * const wpa;
	const char * const dhclient;
	const char * const if_config;
	const char * const iw;
	const char * const killall;
	const char * const wpa_opts;
	int rm_pass;
	
} Defaults;

#endif
