/** wefe-cli
 * 
 * Purpose: This application is a simple proof-of-concept. Use it to
 * interact with wefe running in daemon mode: wefe -d
 * 
 * wefe as a daemon, with a simple cli client, meant to be run as a
 * user, will do you well on a variety of system setups, especially
 * headless ones. You can also take the basic premise, of the server-
 * client relationship, and make a better client, if you want.
 * 
 * Note: In some socket implementations, including python3, you have
 * to send your mode/option as binary data. This is due to the way that
 * C implements its sockets. Further information on this is outside the
 * scope of this application.
 * 
 * Note: If no option is passed, this client will also pass the string
 * "none." This is required on *this* client setup. Other solutions
 * (like python) may not require such an approach.
 */

#include <stdio.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <string.h> 
#define PORT 2116 

int main(int argc, const char *argv[]) 
{ 
	int b = 1024, sock = 0, valread; 
	struct sockaddr_in serv_addr; 
	
	// Send these.
	const char *mode = argv[1]; 
	const char *option = argv[2];
	
	// Server response.
	char buffer[1024] = {0};
	 
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
	{ 
		printf("\n Socket creation error \n"); 
		return -1; 
	} 

	serv_addr.sin_family = AF_INET; 
	serv_addr.sin_port = htons(PORT); 
	
	// Convert IPv4 and IPv6 addresses from text to binary form 
	if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) 
	{ 
		printf("\nInvalid address/ Address not supported \n"); 
		return -1; 
	} 

	if (connect(
		sock,
		(struct sockaddr *)&serv_addr,
		sizeof(serv_addr)) < 0) 
	{ 
		printf("\nConnection Failed \n"); 
		return -1; 
	} 
	
	// Send info to the server.
	send(sock, mode, strlen(mode), 0); 
	sleep(1);
	
	// Accommodate for only one string argument.
	if (option == NULL)
	{
		option = "none";
	}
	send(sock, option, strlen(option), 0);
	sleep(1);
	
	// Print the daemon's response.
	valread = read(sock, buffer, 1024); 
    printf("%s\n", buffer); 
    
	return 0; 
} 
