/*
 * Modified an existing scanner, written by a true hero. Info about the
 * original:
 * 
 *	> wifi-scan-all example for wifi-scan library
 *	> 
 *	> Copyright (C) 2016 Bartosz Meglicki <meglickib@gmail.com>
 *	> 
 *	> This program is free software; you can redistribute it and/or modify
 *	> it under the terms of the GNU General Public License version 3 as
 *	> published by the Free Software Foundation.
 *	> This program is distributed "as is" WITHOUT ANY WARRANTY of any
 *	> kind, whether express or implied; without even the implied warranty
 *	> of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	> GNU General Public License for more details.
 */


// Convert bssid to printable hardware mac address
const char *bssid_to_string(
				const uint8_t bssid[BSSID_LENGTH],
				char bssid_string[BSSID_STRING_LENGTH]);
				
// type: 0=scan, 1=lazy
const char * scan_util(int type);

