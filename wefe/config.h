/**
 * If necessary, edit these based on your system. `struct cmd` is
 * defined in cmd.h
 *
 * Distro-based changes:
 *
 * -	Debian:
 * 		`/sbin/ifconfig` to `/sbin/ip link set dev`
 *
 * -	Alpine:
 * 		`/sbin/dhclient` -> `/sbin/udhcpc -i`
 */

Defaults def =
{
	// Default interface
	"wlan0",

	// Config folder path
	"/etc/wefe/saved",

	// wpa_supplicant path
	"/sbin/wpa_supplicant",

	// dhclient path
	"/sbin/dhclient",

	// ifconfig/ip path and options.
	"/bin/ip",

	// iwconfig path
	"/sbin/iw",

	// Killall path with SIGKILL enabled.
	"/usr/bin/killall -9",

	// Other wpasupplicant options.
	"-B -Dnl80211,wext",

	// Plaintext password: 0=keep, 1=remove.
	1,
};
