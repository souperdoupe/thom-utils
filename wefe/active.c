/**
 * active.c
 * 
 * 	Connect to a wireless network.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "active.h"
#include "cmd.h"
extern Defaults def;


// Connect to a wireless network.
int active(const char * const ssid)
{	
	ActiveArgs a = make_active_args();
	int r = 0;
		
	strcpy(a->ssid, ssid);
	a->r = 0;
	a->bsize = 128;
				
	// Build the dhclient command.
	build_dhc(a);
	
	// Update the network type.
	get_net_type(a);

	// Nonexistent config file.
	if (a->net_type < 0) {
		a->r = -10;
	
	// Config file exists.
	} else {
	
		// Build the connector command (iw/iwconfig/wpasupplicant).
		build_conn(a);
			
		// Returns 0 if both execute. Might split this.
		a->r = system(a->conn) + system(a->dhc);

		// Cnnection failure.
		if (a->r < 0) {
			
			a->r = -11;
		}
	}
	
	r = a->r;
	free(a);
	return r;
};

static ActiveArgs make_active_args()
{
	ActiveArgs args = malloc(sizeof(active_args_t));
	return args;
}

// Set up the dhclient command.
static void build_dhc(ActiveArgs a)
{
	snprintf(a->dhc, a->bsize, "%s %s", def.dhclient, def.iface);
	return;
}

// Determine security type based on config: 0=non-wpa, 1=wpa
static void get_net_type(ActiveArgs a)
{
	// Get the full config filepath.
	int c_len = strlen(def.config_path) + strlen(a->ssid);
	char config[c_len];
	
	snprintf(config, c_len+2, "%s/%s", def.config_path, a->ssid);
		
	// Store the string "open", if applicable.
	char word[5];

	// Open the configuration file.
	FILE *fp = fopen(config, "r");
	
	printf("%s\n", config);

	if (fp == NULL) {
		// Return an error if empty.
		a->net_type = -1;
	
	} else {
	
		// Open network.
		const char * const is_open = fgets(word, 5, fp);
		
		if (strcmp(is_open, "open") == 0)
		{
			a->net_type = 0;
		}
		// Assume WPA.
		else
		{
			a->net_type = 1;
		}
	}
	return;
}

// Build an iw/iwconfig or wpa_supplicant command.
static void build_conn(ActiveArgs a)
{	
	// Open network.
	if (a->net_type == 0)
	{
		set_open_cmd(a);
	}
	
	// WPA/2 network. Caveat: could also be WEP. Revise later.
	else
	{
		set_wpa_cmd(a);
	}
	return;
}

static void set_open_cmd(ActiveArgs a)
{
	// Build an iw/iwconfig command.
	snprintf(a->conn, a->bsize,
		"%s dev %s connect '%s'",
		def.iw,
		def.iface,
		a->ssid);
	return;
}

static void set_wpa_cmd(ActiveArgs a)
{
	// Build a wpa_supplicant command:
	snprintf(a->conn, a->bsize,
		"%s %s -i%s -c'%s'/'%s'",
		def.wpa,
		def.wpa_opts,
		def.iface,
		def.config_path,
		a->ssid);	

	return;
}

