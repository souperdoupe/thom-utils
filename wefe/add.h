/**
 * add.h
 * 
 * Prototypes for add.c (add a network connection)
 */
 

typedef struct add_args_s
{
	char config_cmd[256];
	char pass[64];
	char ssid[33];
	int size;
	int r;

} add_args_t;

typedef add_args_t* AddArgs;
 
int add(const char[33]);
static AddArgs make_add_args();
static void get_password(AddArgs ad);
static void make_config_file(AddArgs ad);
static void make_wpa_config(AddArgs ad);
static void make_open_config(AddArgs ad);
