/*
 * Help message.
 * 
 */

void help()
{
	printf("\nUSAGE: wefe [option] [argument]\n\nNote: If you're adding\
 an SSID with whitespace, put it in single quotes. For example:\
\n\n\twefe add 'my cool hotspot'\n\n");
}
