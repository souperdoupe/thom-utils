wefe - Make a network connection
================================

wefe is a cli-only tool that lets you connect to a network. It's
barebones and will stay that way.

This is the default network assistant in the ThomOS live environment.


Setup
=====

You will need the following dependencies installed:

	libncurses-dev
	libmnl-dev
	wpa_supplicant
	ip or ifconfig
	iw or iwconfig
	dhclient or udhcpc

Run `make clean`, and review `config.h`. You might need to change this
file depending on your installed utilities. For example, Debian users
will want to edit this to use ip instead of ifconfig.


Install
======

Run the following commands:

	make
	sudo make install

If you're rebuilding wefe, run `make clean` first.


Usage
=====

Command
-------

```
wefe [option] [argument]
```

Valid options/arguments
-----------------------

  Option | Description                                  | Argument
---------|----------------------------------------------|---------
  on     | Connect to a saved network                   | ssid name
  add    | Add a hotspot config                         | ssid name
  kill   | Flush all network connections                |
  scan   | Show detailed info about available networks  |
  lazy   | Connect to the first saved network           |


Rationale
=========

This started out as a shell script under 100 lines. Due to concerns
about security (compromising shell scripts), I decided to port it to C.
This decision is to mitigate (though not fully defend against) common
security issues with shell scripts.

The preferred method for using wefe is as a user, and launched with
[sup](https://git.devuan.org/jaromil/sup). This provides a further layer
of security mitigation.


Similar applications
====================

wefe aspires to be the smallest possible implementation for
connectivity. It ships with what I consider to be essential (hence, no
GUI). Other projects offer powerful implementations with perfectly
acceptable user interfaces. They are worth your time, for sure.


Further credits
===============

-   [ceni](https://github.com/antiX-Linux/ceni-antix)

-   [edbarx's simple-netaid](https://git.devuan.org/edbarx/netman)

-   [wifimgr (FreeBSD)](http://opal.com/freebsd/ports/net-mgmt/wifimgr/)

-   [wifi-scan library, by bmegli](https://github.com/bmegli/wifi-scan)


Appendix - Workaround for class variables
=========================================

C was created before object-oriented programming really took off. This
introduces a huge limitation for scoping in C.

wefe scopes each `<filename>.c` file in the following way:

***`<filename>.h`***

An example header might look like:

	typedef struct filename_args_s
	{
		// Declare variables...
	
	} filename_args_t;
	
	typedef filename_args_t* FilenameArgs;
	
	int factory();
	static FilenameArgs create_filename_args();
	static int get_error_code(FilenameArgs a);
	...

To break this down:
	
*	Defines a type based on a struct:

		typedef struct filename_args_s
		{
			// Declare variables...

		} filename_args_t;
		
		typedef filename_args_t* FilenameArgs;
	
*	Prototypes a function that creates a struct pointer:

		int factory();
		static FilenameArgs create_filename_args();
	
*	Also prototypes functions to use that pointer:

		static int get_error_code(FilenameArgs a);

***`<filename>.c`***

With that header in mind, a corresponding `<filename.c>` would include
the following:

*	A method to create the arguments:
	
		static FileNameArgs create_filename_args()
		{
			FilenameArgs f = malloc(sizeof(file_name_t);
			return f;
		}		

*	A "factory method" (function), which initializes the
	"argument" type, passes it to static functions, and and kills it
	at the end:

		int factory()
		{
			FilenameArgs args = create_filename_args();
			
			...
			
			int errors = get_error_code(f);
			
			...
			
			free(args);
			return errors;
		}
	
*	Any number of functions that accept the args pointer. For example:
	
		int get_error_code(FilenameArgs f)
		{
			return f->error_code;
		}
