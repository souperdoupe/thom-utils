/**
 * execute.h
 * 
 * 	Execute commands. Can be called directly, or though the
 *  wefe daemon.
 * 
 * Notes:
 *
 * -	Eventually, no print statements should be used here. Everything
 * 		should be handled with a return code (int). This code should be
 * 		interpreted by the client. The client, in turn, will display
 * 		a message based on the code.
 */
#include <stdio.h>
#include <fcntl.h>	 // open()
#include <unistd.h>	 // close()
#include <string.h>
#include <stdlib.h>	 // realloc()

// Custom headers.
#include "cmd.h"
#include "config.h"
#include "active.h"
#include "add.h"
#include "wifi_scan.h"
#include "scan_util.h"
#include "kill.h"
#include "help.h"
#include "execute.h"


// Handle core executions.
int execute(const char * const mode, const char * const option)
{	
	ExecuteArgs e = make_execute_args();
	int r = 0;
	e->r = 0;
		
	// Update the mode and options, if applicable.
	strcat(e->mode, mode);
	
	// Ignore nonexistent options.
	if (option != NULL || option != "none") {
		strcat(e->option, option);
	}
	
	run_wefe_command(e);

	// Evaluate the return code.
	if (e->r == 0) {
		printf("Done.\n");
	
	} else {
		printf("Errors occurred: code %d\n", e->r);
	
	}
	
	r = e->r;
	free(e);
	return r;
};

static ExecuteArgs make_execute_args()
{
	ExecuteArgs args = malloc(sizeof(execute_args_t));
	return args;
}

// Execute a wefe command.
static void run_wefe_command(ExecuteArgs e)
{
	if(arg_is(e, "on")) {
		run_on(e);
				
	} else if (arg_is(e, "kill")) {
		kill();
		
	} else if (arg_is(e, "add")) {
		run_add(e);
		
	} else if (arg_is(e, "scan")) {	
		const char * s_res = scan_util(0);
		
	} else if (arg_is(e, "lazy")) {
		run_lazy(e);
	
	// Handle errors.	
	} else {
		help();
		e->r = -2;
	}
	
	// Clear data. (This is required, even as a daemon.)
	strncpy(e->mode, "", 33);
	strncpy(e->option, "", 64);
	
	return;
}

// Connect to an SSID (e->option).
static void run_on(ExecuteArgs e)
{
	printf("Attempting to connect...\n");
	
	e->r = kill(def.iface);
	
	sleep(1);
	
	e->r += active(e->option);
	
	if (e->r == 0)
	{	
		printf("Connected to %s.\n", e->option);
	}
	else
	{
		printf("[!]Errors occurred during connection.\n");
	}
	
}

// Add a network by its SSID (e->option).
static void run_add(ExecuteArgs e)
{		
	if (strnlen(e->option, 33) > 32) {
		
		printf("Invalid SSID length.\n");
		e->r = -1;
	
	} else {
	
		e->r = add(e->option);
		
		if (e->r == 0) {
		
			printf("Added %s\n", e->option);
			kill(def.iface);
			active(e->option);
		
		} else {
	
			printf("Errors occurred while adding %s.\n", e->option);
		
		}
	}
	return;
}

// Automatically connect to a network.
static void run_lazy(ExecuteArgs e)
{	
	// Connect only if everything dies correctly (how morbid).
	e->r = kill();
		
	if (e->r == 0) {
	
		printf("Lazy mode: trying to find initiative...\n");
		
		// Update the SSID value.
		strcpy(e->option, scan_util(1));
		
		e->r = active(e->option);
	}
	return;
}

// Produce a true(1)/false(0) result.
static int arg_is(ExecuteArgs e, const char * const eval_mode)
{
	int result = 0;
	
	if (strcmp(e->mode, eval_mode) == 0) {		
		result = 1;
	
	} else {
		result = 0;
	
	}
	return result;
}
