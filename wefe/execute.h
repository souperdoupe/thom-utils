/**
 * execute.h
 * 
 * Prototypes for execute.c
 */
 
typedef struct execute_args_s
{
	char mode[33];
	char option[64];
	int r;
	
} execute_args_t;

typedef execute_args_t* ExecuteArgs;

// Prototypes used in this header.
int execute(const char * const mode, const char * const option);
static ExecuteArgs make_execute_args();
static void run_wefe_command(ExecuteArgs e);
static void run_on(ExecuteArgs e);
static void run_add(ExecuteArgs e);
static void run_lazy(ExecuteArgs e);
static int arg_is(ExecuteArgs e, const char * const eval_mode);
