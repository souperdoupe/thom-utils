/*
 * daemon.c
 * 
 *	Execute wefe commands using sockets (server-client model).
 * 
 *  Enables wefe to be used as an unprivileged user. Use `wefe-cli` to
 *  interact with the daemon.
 * 
 * TO-DO:
 * 
 * 	-	Return an error code (int) instead of a string pointer.
 * 
 * ISSUES:
 * 
 * 	-	Sending a string reply gets cut-off by the client. Printing
 * 		the reply string (pointer) on the server-side shows that it
 * 		the pointer holds the correct data. But no client implementation
 * 		displays more than the first four characters.
 */

#include <stdio.h>
#include <unistd.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>

#include "daemon.h"
#include "execute.h"

#define PORT 2116


void daemonized() 
{
	DaemonArgs d = make_daemon_args();
	
	build_connection(d);
	
	// Listen indefinietly.
	while (1)
	{
		// Reset variables.
		clear_messages(d);
		
		// Wait for a client message.
		wait_for_message(d);

		// Execute the action.
		d->status = execute(d->mode, d->option);	
		
		// Send a response to the client.
		send_reply(d);
						
		// Close the socket.
		cleanup(d);
	}
	
	return;
} 

DaemonArgs make_daemon_args()
{
	DaemonArgs args = malloc(sizeof(daemon_args_t));
	
	// Initialize values.
	args->opt = 1; 
	args->addrlen = sizeof(args->address); 
	
	// Intentionally setting to empy/blank values.
	memset(args->mode, 0, 1024);
	memset(args->option, 0, 1024);
	args->reply = NULL;
		
	return args;
}

void build_connection(DaemonArgs d)
{
	// Creating socket file descriptor 
	if ((d->server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
	 
		perror("socket failed"); 
		exit(EXIT_FAILURE); 
	} 
	
	// Forcefully attaching socket to the port. 
	if (setsockopt(d->server_fd, SOL_SOCKET,
					SO_REUSEADDR | SO_REUSEPORT,
					&d->opt, sizeof(d->opt))) {	
		 
		perror("setsockopt"); 
		exit(EXIT_FAILURE); 
	}
	
	d->address.sin_family = AF_INET; 
	d->address.sin_addr.s_addr = INADDR_ANY; 
	d->address.sin_port = htons( PORT ); 
	
	// Forcefully attaching socket to the port. 
	if (bind(d->server_fd, (struct sockaddr *)&d->address, 
								sizeof(d->address))<0) {
		 
		perror("bind failed"); 
		exit(EXIT_FAILURE); 
	} 
	
	if (listen(d->server_fd, 3) < 0) {
		 
		perror("listen"); 
		exit(EXIT_FAILURE);
	}
}

void cleanup(DaemonArgs d)
{
	clear_messages(d);
	close(d->new_socket);
	sleep(1);
}

void clear_messages(DaemonArgs d)
{
	// Will remove these when I determine which is a better way.
	//memset(d->mode, '\0', 1024);
	//memset(d->option, '\0', 1024);
	strncpy(d->mode, "", 1024);
	strncpy(d->option, "", 1024);
	printf("AFTER WIPE: (%s : %s)\n", d->mode, d->option);
	
	d->reply = NULL;
	
	return;
}

void send_reply(DaemonArgs d)
{
	// Determine what kind of response to send.
	if (d->status >= 0) { 
		d->reply = "Success";
		
	} else {
		d->reply = "Error";
	
	}
	
	// Send a reply.
	send(d->new_socket, d->reply, strlen(d->mode), 0);
	
	return;
}

void wait_for_message(DaemonArgs d)
{
	d->new_socket = accept(d->server_fd, (struct sockaddr *)&d->address,
						(socklen_t*)&d->addrlen);

	// Get command from the client.
	d->valread = read(d->new_socket, d->mode, 1024);
	sleep(1);
	d->valread = read(d->new_socket, d->option, 1024); 
	sleep(1);
	
	return;
}
