# Build thomos

*Note: The build scripts are still a serious WIP. Please ignore the mess.*

This folder contains what you need to build thomos yourself,
if you so desire.  Please remember that thomos is designed for:

-	Minimalism

-	Nonpersistence

Some of these defaults may not be what you want.  If that's the case
modify them.  Or don't.  I don't care.

Pay me to care.

# Rationale

The script contains a basic setup to debootstrap, then chroot into
the debootstrapped environment.

Once in the chroot, the script itself cannot do anything directly.

To solve this, a shit-ton of functions are created, then passed as a
single command.  Once the build command finishes, the chroot exits,
and the script cleans everything up for you.

# Structure

File | Description
---|---
build-thomos.sh | Execute this to build thomos
apps.list | Complete list of applications for apt-get to install
build.config | Variables for building (eg, release, architecture)
