# Suckless

WIP.

The basic idea here is:

1.	Compare version against dpkg.
1.	If needed, download, and rebuild.
1.	Create a .deb file.
1.	Install .deb file at install time.

## Contents

Name | Description
---|---
dwm | `config.h` with thomos settings applied, and corresponding `.deb`
sup | `config.h` with current values of thomos scripts.
