/* See LICENSE file for copyright and license details. */

/* User and group to run as */
#define SETUID 0
#define SETGID 0

/* sup authorizations
 *
 * The format is as follows:
 * - UID allowed to run the command (-1 means anyone)
 * - Alias/command used to call the executable
 * - Path to the executable to run
 * - SHA256 checksum of the executable
 */
static struct rule_t rules[] = {

	{ -1, "bright", "/tmp/bright",
		"d386e75bd5a53d4a60b6b036faa1845fabeb211ae3d0bca09d9a148a4248def5"},
};
