#!/bin/bash


# Installer script (ala C, Java).
main()  # Args: str target_directory, str install_type, str build
{
	# In lieu of class variables.
	local dir="$1"
	local install_type="$2"
	local build_type="$3"

	# Pull config values and build functions.
	source "./build.config" && \
	source "./build.d/install.headers"
	setUp "$dir" && \
	msg "Loaded build.config and build.headers" || err;

	# Evaluate build numbers
	local old_build=$(cat ./build.number) && \
	local build_no=$(getBuild $build_type) && \
		msg "Attempting to build ver.$build_no $build_type" || \
		err;

	# Do the installation.
	action "$install_type" && \
	chown $USER:$USER "$dir"/home/snapshot/* && \

	# If successful, update build number, and relocate iso.
	`echo $build_no > ./build.number` || \

	# On failure, revert to the old build number
	$(msg "Reverting build number $build_no -> $old_build" && \
	echo $old_build > ./build.number);

	# Regardless, clean up mount points.
	msg "Unmounting dev, proc, sys..." && \
	chrootTool unmount;
}

# Run script.
main "$1" "$2" "$3";
