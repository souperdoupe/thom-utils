# Configs

This table explains each item in the `configs` directory. Everything
has been customized for the Thomos project. Love it or fork it.

Name | Description | Usage
---|---|---
dwm | DWM .deb and config.h | Install, or rebuild with config.h
environment | Messages, like os-release/motd | Antequated
firefox | Files for your firefox user folder | Place in `.mozilla/firefox/*.default`
sup | Suckless permissions manager | Place conifg.h in your source folder, and rebuild.
ksh.kshrc | User settings for the Kornshell/95 | `cp ksh.kshrc ~/.kshrc`
tint2rc | Custom tint2 taskbar (Openbox) | Install tint2, then `cp tint2rc ~/config/tint2`
Xdefaults | Make URXVT look pretty | `cp Xdefaults ~/.Xdefaults`
