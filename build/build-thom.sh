#!/bin/bash

# Script to built thomos from chroot.

# Read from config. (Addresses things like releases, etc.)
setUp()  # Args: taret_dir
{
	# Test and create target directory, if needed.
	if [ "$1" ]; then
		dir="$1"
	else
		echo "Set target directory." && \
		exit 0
	fi
	# Confirm loaded values.
	echo "
	release	: $release
	arch	: $arch
	target	: $dir
	mirror	: $repo_url
	" 2> /dev/null
	# Give a few seconds to review.
	sleep 2
}

# Shorthand for announcing messages.
msg()  # Args: str content
{
	echo "[thomos] $1";
}

# Handle errors.
err()
{
	msg "Errors occurred" && \
	exit 1;
}

# Update release number.
updateBuildNo()  # Args: str build|major|minor
{
	# Get the old build number.
	local build_no=$(cat ./build.number)  # ie, 2.34.5
	# Evaluate how to update it.
	case $target in
		build)
			local new=$(echo $build_no | awk -F. '{print $1}') && \
			local new=$(expr $new + 1) && \
			local new_build=$new.0.0
		;;
		major)
			local build=$(echo $build_no | awk -F. '{print $1}') && \
			local new=$(echo $build_no | awk -F. '{print $2}') && \
			local new=$(expr $new + 1) && \
			local new_build=$build.$new.0
		;;
		minor)
			local build=$(echo $build_no | awk -F. '{print $1}') && \
			local build=$(echo $build_no | awk -F. '{print $2}') && \
			local new=$(echo $build_no | awk -F. '{print $3}') && \
			local new=$(expr $new + 1) && \
			new_build=$build.$major.$new
		;;
	esac && \
	# Write to file.
	echo "$new_build" > ./build.number && \
	# Return new_build.
	echo $new_build
}

# Shorthand to test if an app exists, and
# install it if it isn't there.
getApp()  # Args: str app_name
{
	if [[ ! $("$1" 2> /dev/null) ]]; then
		apt-get install "$1";
	fi || err;
}

# Make a list of all applications.  Needed for 
# updates and debootstrap.
makeAppList()
{
	# List all applications.
	local app_list=();
	# Update repos.
	chroot "$dir" /usr/bin/apt-get update && \
		chroot "$dir" /usr/bin/apt-get dist-upgrade;
	# Loop through all lists.
	for list in "./apps.d/*.list";
	do
		local list_apps="$(cat $list)";
		for app in $list_apps;
		do
			# Append to a list.
			app_list+=($app);
			unset app;
		done;
		unset list;
	done && \
	# Return val.
	echo ${app_list[@]}
}

# Make sure build dependencies are installed.
getDeps()
{
	local depends_list=$(cat build.depends) && \
	for dependency in $depends_list;
	do
		getApp $dependency && \
		unset dependency;
	done
}

# Debootstrap to a target directory.
debootThom()
{
	# Create and enter the target directory.
	if [ ! -d "$dir" ];
	then
		mkdir -p $dir
	fi
	# Prompt if the target directory contains content.
	if [ "$(ls -A $dir)" ];
	then
		msg "Target directory is not empty. Continue? N/y"
		read cont
		case $cont in
			# Debootstrap anyway.
			Y|y)
				msg "Debootstrapping a non-empty folder."
			;;
			# Skip debootstrap by default.
			*)
				msg "Skipping debootstrap." && return 0
			;;
		esac
	fi
	# Default behavior if the directory exists and is empty.
	msg "Starting debootstrap."
	# App list, comma-separated values.
	local app_list=$(makeAppList | sed 's/ /,/g') && \
	debootstrap \
		--variant="minbase" \
		--include="linux-image-$arch,nano,$app_list" \
		--arch "$arch" \
		"$release" \
		"$dir" "$repo_url" && \
	# Success/error message.
	msg "Debootstrap complete." || err;
}

# Chroot into said environment.
# NOTE: Need to ignore 'already mounted'/'already dismounted'
# and only treat things like, "couldn't mount or w/e" as
# erors.
chrootTool()  # Args: [mount|unmount]
{
	# Set up an array for success messages.
	local ok=();
	# Legit need to write the loop with an exception for proc.
	local m_opt=("-t" "-o" "-o");
	local m_type=("proc" "dev" "sys");
	# Mount the target chroot.
	if [[ "$1" == "mount" ]];
	then
		mount -t proc proc $dir/proc;
		mount -o bind /dev $dir/dev;
		mount -o bind /sys $dir/sys;
		# Will make more sense later.,,
		msg "Mounted proc, dev, and sys. " || err;
	# Unmount everything.
	elif [[ "$1" == "unmount" ]];
	then
		umount "$dir/sys";
		umount $dir/dev;
		umount $dir/proc;
		msg "Directories unmounted." || err;
	fi
}

# Install thom-utils/scripts.
thomInstaller()
{
	for i in ../scripts/*;
	do
		cp $i "$dir"/usr/local/bin;
	done
}

# Configure user/root settings.
makeUser()  # Args: user_name
{
	local user="$1"
	# Change root password.
	passwd root
	# Add a new user.
	useradd -m $user
	passwd $user
	# Set the locale.
	localectl set-locale "$locale"
}

# Configure environment settings.
makeEnv()
{
	# Set the hostname.
	echo "$iso_hostname" > "$dir"/etc/hostname && \
	# Update /etc/os-release, with build info.
	local os_release="$(cat ./env.d/os-release)" && \
		echo "$os_release" > "$dir"/etc/os-release && \
	# Update /etc/motd, with build info.
	local motd="$(cat ./env.d/motd)" && \
		echo "$motd" > "$dir"/etc/motd && \
	msg "Success" || err;
}

# Install applications from apt-get.
# (for chroot)
aptInstaller()
{
	# Fetch the list of apps.
	local app_list=$(makeAppList);
	# Install everything in chroot.
	chroot "$dir" \
		/usr/bin/apt-get install \
			--no-install-recommends \
			-y \
			"${app_list[@]}" && \
	msg "Installed all applications" || err;
}

# Patch, build, and install custom suckless tools.
# (for chroot)

# [WIP] Patch, build, install alternative init.

# Build the image.
genImage()
{
	# Clean everything up.
	rm -r "$dir"/tmp/*;
	shred --zero \
		"$dir"/home/thomos/.bash_history \
		"$dir"/root/.bash_history;
	# Create the image.
	chroot "$dir" \
		refractasnapshot && \
	# Push image to final location (configurable).
	msg "Generated ISO." || err;
}
