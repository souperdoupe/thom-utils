#!/bin/sh

# thomos installer
# Best when used with a minimalist base and five cups of black coffee.
# WIP - Better management for installing to a chroot environment
#     - Possible migration to python3 (dictionaries would be helpful)

set -e
set -u
#set -o pipefail # Only works with bash.
root=""
wefe="wefe-sup"
user_name="$USER"
user_home="$user_name"
chroot=false
chroot_env=""

optErrMsg(){
	echo "$(basename $0) [-r /path/to/newroot] [-i scripts|wefe|power|configs|full]" && \
	exit 1
} # Update this message if you change any installer options.

chrootCmd(){
# $1 should be either "mount" or "umount"
	case $1 in 
	mount)
		$1 -t proc proc $chroot_env/proc && \
		$1 -o bind /sys $chroot_env/sys && \
		$1 -o bind /dev $chroot_env/dev && \
		$1 -o bind /dev/pts $chroot_env/dev/pts && \
		cp /etc/resolv.conf $chroot_env/resolv.conf ;;
	umount) 
		umount $chroot_env/proc $chroot_env/sys \
		$chroot_env/dev/pts $chroot_env/dev && \
		rm $chroot_env/resolv.conf ;;
	esac
}

installScripts(){
	echo "Installing scripts..." && sleep 2s && \
	cp ./scripts/* "$root"/usr/local/bin && \
	echo "[SUCCESS]"
}

installWefe(){
	# WIP: using wefe as a user.
	echo "Preparing to install wefe (thomos wifi/network tool)..." && \
		sleep 2s && \
	# Install dependencies
	# I've gotta clean this section up
	if [ "$chroot" = true ]; then
		chrootCmd mount && \
		chroot "$chroot_env" /bin/sh -c \
			"sudo apt-get update && \
			sudo apt-get -y install --no-install-recommends wpasupplicant"
		chrootCmd umount;
	else sudo apt-get update && \
		sudo apt-get -y install --no-install-recommends wpasupplicant
	fi
	sudo apt-get -y install --no-install-recommends wpasupplicant;
	wefeType(){
		echo "Select how wefe should manage wifi by default (/usr/local/bin/wefe): 
		1: wpasupplicant (default)
		2: network/interfaces"
		read mgmt
		case $mgmt in
			"1") wefe="wefe-sup" ;;
			"2") wefe="wefe-if" ;;
			# Prevent script from breaking on user input.
			*) echo "Invalid option." && wefeType ;;
		esac; }
	echo "Installing $wefe"... && \
	# Copy and update permissions
	cp ./wefe/"$wefe" "$root"/usr/local/bin/wefe && \
	chrootCmd mount && 
	chroot $chroot_env /bin/sh -c \
		"chmod +x "$root"/usr/local/bin/wefe && \
		chown $user_name:thomos /usr/local/bin/wefe"
	chrootCmd umount
	# Create config directories.
	mkdir -p "$root"/etc/wefe/"$wefe"/exops && \
	cp ./wefe/scan.awk "$root"/etc/wefe && \ 
	echo "[SUCCESS]"
}

installPower(){
	echo "Preparing to configure thomos power management..." && \
		sleep 2s && \
	# Get power dependencies
	chrootCmd mount && \
		chroot $chroot_env /bin/sh -c \
			"sudo apt-get update && \
			sudo apt-get -y install --no-install-recommends pm-utils acpid suckless-tools" && \
	chrootCmd umount
	# Set-up folders in case they weren't with install
	mkdir -p "$root"/etc/pm/sleep.d "$root"/etc/acpi/events
	# Move the configurations to their proper homes
	cp ./power/00lock "$root"/etc/pm/sleep.d && \
		chmod +x "$root"/etc/pm/sleep.d/00lock &&\
	cp ./power/lid.sh "$root"/etc/acpi && \
		chmod +x "$root"/etc/acpi/lid.sh && \
	cp ./power/lid "$root"/etc/acpi/events && \
	cp ./power/tbattery.sh /usr/local/bin/tbattery &&\
		chmod +x /usr/local/bin/tbattery
	echo "[SUCCESS]"
}

installConfigs(){
	echo "Preparing to install thomos configs..." && \
		sleep 2s && \
	# Install dependencies
	# I've gotta clean this section up
	if [ "$chroot" = true ]; then
		chrootCmd mount && \
		chroot "$chroot_env" /bin/sh -c \
			"sudo apt-get update && \
			sudo apt-get -y install --no-install-recommends ksh tint2"
		chrootCmd umount;
	else sudo apt-get update && \
		sudo apt-get -y install --no-install-recommends ksh tint2
	fi
	# Grab user name for home configs
	# Place accordingly
	mkdir "$root"/home/"$user_home"/.config && \
	cp ./configs/tint2rc "$root"/home/"$user_home"/.config/tint2 && \
	cp ./configs/ksh.kshrc "$root"/home/"$user_home"/.kshrc && \
	cp ./configs/Xdefaults "$root"/home/"$user_home"/.Xdefaults && \
	cp -r ./themes/ganesh "$root"/usr/share/themes
	echo "[SUCCESS]"
}

installSession(){
	cwd="$(pwd)"
	echo "Preparing to install session configs..." && \
	cd ./session && \
	for i in rc.shutdown rc.startup; do
		cp $i /etc; done && \
	for i in session shutdown reboot; do
	    cp $i /sbin; done && \
	cp rc.init /bin && \
	cd $cwd
}

# DO THE THING.
while getopts 'r:i:u:ch:' OPTION; do
	case "$OPTION" in
		# Set a user's name instead of manually doing so along the way
		u) user_name="$OPTARG" && echo "user is $user_name" ;;
		# Change root.  Helpful for installing to a chroot env.
		r) root="$OPTARG" && echo "root directory is $root";;
		# Choose install type.
		i) case "$OPTARG" in
				scripts) installScripts ;;
				wefe) installWefe ;;
				power) installPower ;;
				configs) installConfigs ;;
				session) installSession ;;
				all) installScripts && installWefe && installPower && \
					installConfigs && installSession &&
					echo "[thomos] successfully hatched.\nWhat hast thou done?" ;;
				*) optErrMsg ;;
			esac ;;
		# Chroot options
		c) chroot_env="$root" && chroot=true && \
			echo "chroot environment: $chroot_env";;
		h) home_folder="$OPTARG" ;; # If you want to change home dir.
		?) optErrMsg ;;
	esac
done
mount | grep "$root"
shift "$(($OPTIND -1))"
