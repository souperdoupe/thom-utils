# tsession

**Note**: This section is documentation for tinkering. It is not stable and may
break your system. Use this at your own risk.

This is documentation for an init concept. It uses Suckless.org's sinit to launch the
most basic session in the modern tech world. You might find this useful if you are
interested in software minimalism, pushing the envelope, etc.

# Theory of operation

The current setup is built for this workflow. (See the [caveats](#caveats) section
for more info.)

1.  A minimal environment, with SysV's init, is installed and booted. (This gives you
    all SysV "runlevel" scripts, without the need to make them from scratch.
    Modify these as needed.)

1.  On the target system, sinit is compiled with no changes to `config.h`.

1.  Copy the sinit build to wherever `init` is already installed. Rename `sinit` ->
    `init` to overwrite SysV's init.

1.  Install the "session" scripts.

1.  Reboot the target system.

1.  During the next boot, by default, sinit will execute `/sbin/rc.init`.

1.  This project's rc.init file executes the startup script, and the tsession script.

1.  The tsession script just creates a loop for a getty prompt. The loop also looks for
    changes to the session; this data is stored in a simple text file. This maintains 
    the session itself.

1.  When you're ready, run the "shutdown" or "reboot" scripts. This will send a SIGKILL
    to tession.

1.  Once the loop exits, `/sbin/rc.shutdown` is executed. System turns off or reboots.

That's it.

# Caveats

-   There is virtually no error-checking

-   Running critical tasks with shell scripts can introduce serious vulnerabilities

-   No runlevel support is used (not an issue for me, as OpenBSD doesn't have
    runlevels, but it's worth noting)

-   Due to "borrowing" SysV scripts, Xorg may not boot on some setups

-   No security or penetration testing has been performed or documented

-   Using SIGKILL from a PID file is neither a serious nor rigorous way to stop a
    system's session

# Status

The following is a list of test devices. The only working setup was a minimal
Virtualbox session.

Device | Status | Description
---|---|---
VirtualBox | Working | No reported errors.
RaspberryPi 3 | Not working | Stops at the bootloader
Lenovo X230 | Untested

# See also

-   [sinit](https://core.suckless.org/sinit/) - Project homepage

-   [s6](https://www.skarnet.org/software/s6-linux-init/) - This project is far 
    more minimalist than SysV, and only somewhat bulkier than sinit. It is,
    however, far more stable than this setup.