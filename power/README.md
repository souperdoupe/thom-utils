# ThomOS Power Management

Unlike literally most Linux distros, ThomOS uses no centralized power
manager.  Instead, it uses the acpi directly, just like
other projects' power managers do. It is based on conversations here: 
https://dev1galaxy.org/viewtopic.php?id=774.

## Dependencies

- acpi
- pm-utils (optional)
- slock (optional)

# Structure

## tbattery

The `tbattery` program performs a few major functions you'd expect from
a power manager:

1.	Performing a power action after the system is inactive for a set
	amount of time.
	
2.	Adjusts the screen brightness.

3.	Sends power notifications to the console and/or desktop.

Note that tbattery started as a shell script. `tbattery.c` basically
just calls shell commands. Think of it as a shell script with a little
more hardening.

## ACPI

By understanding the relationship between these files and directories,
one can manually perform many of the functions that a gui power manager
does:

File | Location | Description
---|---|---
Listener | `/etc/acpi/events/lid` | Tells acpi to listen for lid events, and run lid.sh
Lid actions | `/etc/acpi/lid.sh` | Commands that run when lid actions are performed (in ThomOS, runs `pm-suspend` or `pm-hibernate`)
Lock actions | `/etc/pm/sleep.d/00lock` | Defines the default display as a variable, and tells pm-suspend/hibernate to run an app like `slock` whenever suspending or hibernating

This is the default behavior.  Each of these can be modified or removed.

# Installation

1.	Install dependencies. Debian example:
	
	```
	sudo apt-get update && \
	sudo apt-get -y install --no-install-recommends \
		pm-utils \
		acpid \
		suckless-tools \
		gcc
	```

2.	Run `make` and `make install`.

## Using graphical apps with acpi scripts

There are two things necessary when running graphical apps with any
acpi scripts.

Task | Config syntax | Description
---|---|---
DISPLAY variable | `export DISPLAY=:0` | It is necessary that acpi knows exactly which display you're running it on.
Run as user | `su -c [command] [user]` | Since acpi runs as root, it is necessary to run anything that "should" be ran as a user to 

ThomOS uses the following set of commands to ensure the proper display
is used, and that a command is ran as a user.  Its usage can be observed
in the file 00lock:

```
# Define your display

export DISPLAY=:0

# Run a specified program as user`

run(){ su -c "$1" thomos; }
```

# Caveats

-	Currently, tbattery is invoked as root only. This can lead to
	exploitation under the wrong circumstances. A server-client model
	is currently in development.
