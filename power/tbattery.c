/* 
 * tbattery - Battery watcher for linux systems
 * 
 * Written by souperdoupe: https://gitlab.com/souperdoupe
 * 
 * Automates battery commands when the power level drops.
 * 
 * Notes:
 * 
 * - For some reason, the file reads the status as a 12-character item.
 *   It does not correctly loop through array elements. This makes
 *   strcmp() return a value of 10, instead of 0. Thus, at this time,
 * 	 the program accepts 10 as the "matches" value.
 * 
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <libnotify/notify.h>

struct Afk
{
	char checker_cmd[256];
	char action[256];
	int threshold;
	int last_input;
};

struct Batt
{
	char status[64];
	int capacity;
	char action[256];
	int threshold;
	const char root[128];
	char status_path[256];
	char capacity_path[256];
};

// Could dump these in a header file.
struct Batt batt = 
{
	"uninitialized",
	0,
	"/usr/sbin/pm-suspend",
	12,
	"/sys/class/power_supply/BAT0/",
};

struct Afk afk =
{
	"/usr/bin/xssstate -i",
	"/usr/sbin/pm-suspend",
	840,
};

void notify(char *message)
{
	notify_init("[battery]");
	NotifyNotification * Hello = notify_notification_new(
		"[battery]", message, "dialog-information");
	notify_notification_show(Hello, NULL);
	g_object_unref(G_OBJECT(Hello));
	notify_uninit();
}

/*
 * Sleep utilities.
 */
void get_paths()
{
	snprintf(batt.status_path, 256, "%s%s", batt.root, "status");
	snprintf(batt.capacity_path, 256, "%s%s", batt.root, "capacity");
}

void get_status()
{
	FILE *stat_file = fopen(batt.status_path, "r");
	// Store file contents as a character array (string).
	fgets(batt.status, 64, stat_file);
	fclose(stat_file);
}

void get_capacity()
{
	FILE *capacity_file = fopen(batt.capacity_path, "r");

	// Store the file contents as an int.
	fscanf(capacity_file, "%d", &batt.capacity);
	fclose(capacity_file);
}

// Determine if the battery level is above or below the threshold.
void low_battery()
{
	// Take action if the system is discharging and below the threshold.
	if((strcmp(batt.status, "Discharging") == 10) && \
		(batt.capacity < batt.threshold))
	{
		notify("Low power.\n");
		sleep(30);
		
		// Check once more to see if it's discharged...
		get_status();
		
		// ...if so, take action.
		if(strcmp(batt.status, "Discharging") == 10)
		{
			printf("LOW POWER suspend.\n");
			system(batt.action);
		}
	}
}

/*
 * Timeout utilities.
 */

void timeout()
{
	// Run xssstate and get its output.
	FILE *fp;
	fp = popen(afk.checker_cmd, "r");
	fscanf(fp, "%d", &afk.last_input);
	fclose(fp);
	
	afk.last_input = afk.last_input / 1000;
	
	// Sleep if it hasn't recieved input in awhile.
	if ((afk.last_input > afk.threshold) && \
		(strcmp(batt.status, "Discharging") == 10))
	{
		// Suspend, hibernate, etc.
		printf("TIMEOUT suspend.\n");
		system(afk.action);
	}
}

// Like a daemon.
int main()
{
	// Update system paths, etc.
	get_paths();
	
	// Initial message.
	notify("Starting battery manager...\n");
	
	// Loop infinitely.
	while(1)
	{
		// Get status and percentage.
		get_status();
		get_capacity();

		// Evaluate if the system is inactive.
		timeout();
		
		// Evaluate if the battery drops below the threshold.
		low_battery();
		
		// Keep waiting.
		sleep(10);
	}

	// Closing message.
	notify("Stopping battery manager.\n");
	return 0;
};
